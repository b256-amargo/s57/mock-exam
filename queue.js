let collection = [];

// Write the queue functions below.

function print() {
	// output all the elements of the queue
	return collection
};

function enqueue(element) {
	// add an element to the rear of the queue 
	collection.push(element)
	return collection
};

function dequeue() {
	// remove an element at the front of the queue
	collection.shift()
	return collection
};

function front() {
	// show the element at the front of the queue
	return collection[0]
};

function size() {
	// show the total number of elements  
	return collection.length
};

function isEmpty() {
	// return a Boolean value describing whether the queue is empty or not
	if(!collection){
		return true
	}
	else{
		return false
	}
};

module.exports = {
	collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};

